# README #

### What is this repository for? ###

A genetic algorithm approach to the 8 queens problem. This repository also includes an attempt at the problem in Haskell that I couldn't get to perform efficiently. This is probably due in part to my knowledge of monads being very limited.